-- Answers to exercise 2 questions
SELECT unidb_students.fname, unidb_students.lname, unidb_attend.dept, unidb_attend.num FROM unidb_students
INNER JOIN unidb_attend ON unidb_students.id = unidb_attend.id WHERE unidb_attend.dept = "COMP";

SELECT unidb_students.fname, unidb_students.lname FROM unidb_students
    INNER JOIN unidb_courses ON unidb_students.id = unidb_courses.rep_id WHERE unidb_students.country != "NZ";

SELECT unidb_lecturers.office FROM unidb_lecturers
INNER JOIN unidb_teach ON unidb_lecturers.staff_no = unidb_teach.staff_no WHERE unidb_teach.dept = "COMP" AND unidb_teach.num = 219;

SELECT DISTINCT unidb_students.fname, unidb_students.lname FROM unidb_students
INNER JOIN unidb_attend ON unidb_students.id = unidb_attend.id
INNER JOIN unidb_teach ON CONCAT(unidb_attend.dept, unidb_attend.num) = CONCAT(unidb_teach.dept, unidb_teach.num)
INNER JOIN unidb_lecturers ON unidb_teach.staff_no = unidb_lecturers.staff_no WHERE unidb_lecturers.fname = "Te Taka";

SELECT DISTINCT a.fname, a.lname, b.fname, b.lname FROM unidb_students AS a, unidb_students AS b WHERE a.mentor = b.id;

SELECT unidb_lecturers.fname, unidb_lecturers.lname FROM unidb_lecturers WHERE unidb_lecturers.office LIKE "g%"
UNION
    SELECT unidb_students.fname, unidb_students.lname FROM unidb_students WHERE country = "NZ";
