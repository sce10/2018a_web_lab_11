DROP TABLE IF EXISTS soccer_leaguef;
DROP TABLE IF EXISTS soccer_leaguestandingf;
DROP TABLE IF EXISTS soccer_teamf;
DROP TABLE IF EXISTS soccer_teamcompositionf;
DROP TABLE IF EXISTS soccer_playerf;

CREATE TABLE soccer_leaguef (
    leagueid int NOT NULL,
    leaguename varchar(50) NOT NULL,
    PRIMARY KEY (leagueid)
);

CREATE TABLE soccer_teamf (
    teamid INT NOT NULL,
    teamname VARCHAR(50) NOT NULL,
    teamhomecity VARCHAR(50) NOT NULL,
    captainid INT NOT NULL,
    pointsacc INT NOT NULL,
    leagueid INT NOT NULL,
    PRIMARY KEY (teamid, captainid, leagueid),
    FOREIGN KEY (leagueid) REFERENCES soccer_leaguef(leagueid)
--    FOREIGN KEY (captainid) REFERENCES soccer_player(playerid)

);

CREATE TABLE soccer_playerf (
    playerid INT NOT NULL,
    playerfname VARCHAR(30) NOT NULL,
    playerlname VARCHAR(30) NOT NULL,
    playerage INT DEFAULT NULL,
    playernationality VARCHAR(30) DEFAULT NULL,
    teamid INT NOT NULL,
    PRIMARY KEY (playerid),
    FOREIGN KEY (teamid) REFERENCES soccer_teamf(teamid)
);

CREATE TABLE soccer_leaguestandingf (
    leagueid INT NOT NULL,
    teamid INT NOT NULL,
    rank INT NOT NULL,
    PRIMARY KEY (leagueid, teamid),
    FOREIGN KEY (leagueid) REFERENCES soccer_leaguef(leagueid),
    FOREIGN KEY (teamid) REFERENCES soccer_teamf(teamid)

);



CREATE TABLE soccer_teamcompositionf (
    teamid INT NOT NULL,
    playerid INT NOT NULL,
    role VARCHAR(50) NOT NULL,
    PRIMARY KEY (teamid, playerid),
    FOREIGN KEY (teamid) REFERENCES soccer_teamf(teamid),
    FOREIGN KEY (playerid) REFERENCES soccer_playerf(playerid)
);



INSERT INTO soccer_leaguef (leagueid, leaguename) VALUES
    (123, 'superleague'),
    (456, 'ultraleague');

INSERT INTO soccer_teamf (teamid, teamname, teamhomecity, pointsacc, leagueid, captainid) VALUES
    (123, 'SillyGeese', 'Hamilton', 666, 123, 1),
    (456, 'Buzzards', 'Rotorua', 707, 123, 4),
    (789, 'Wildcats', 'Wellington', 123, 456, 7);

INSERT INTO soccer_playerf (teamid, playerid, playerfname, playerlname, playerage, playernationality) VALUES
    (123, 1, 'Fred' ,'Phelps', 21, 'Iceland' ),
    (123, 2, 'Jack' ,'Jackson', 22, 'Jamaican' ),
    (123, 3, 'Kim' ,'Kardashian', 23, 'New Zealand' ),
    (456, 4, 'Kanye', 'West', 24, 'Sweden'),
    (456, 5, 'Gigi', 'Hadid', 25, 'Antarctica'),
    (456, 6, 'Robert', 'DeNiro', 26, 'Botswana'),
    (789, 7, 'Kerry', 'OrangeJuice', 27, 'Thailand'),
    (789, 8, 'Pedro', 'Paddington', 28, 'Australia'),
    (789, 9, 'Sebastian', 'TheSly', 29, 'Estonia');

INSERT INTO soccer_leaguestandingf (leagueid, teamid, rank) VALUES
    (123, 123, 1),
    (123, 456, 2),
    (456, 789, 1);

INSERT INTO soccer_teamcompositionf (teamid, playerid, role) VALUES
    (123, 1, 'forward'),
    (123, 2, 'back'),
    (123, 3, 'center'),
    (456, 4, 'striker'),
    (456, 5, 'leftwing'),
    (456, 6, 'rightwing'),
    (789, 7, 'keeper'),
    (789, 8, 'wing'),
    (789, 9, 'mascot');

ALTER TABLE soccer_teamf ADD FOREIGN KEY (captainid) REFERENCES soccer_playerf(playerid);
