-- Answers to exercise 1 questions
SELECT DISTINCT unidb_courses.dept FROM unidb_courses;

SELECT DISTINCT unidb_attend.semester FROM unidb_attend;

SELECT DISTINCT CONCAT(unidb_attend.dept, unidb_attend.num) AS Course FROM unidb_attend;

SELECT unidb_students.fname, unidb_students.lname, unidb_students.country FROM unidb_students ORDER BY unidb_students.fname;

SELECT CONCAT(unidb_students.fname, unidb_students.lname), unidb_students.mentor FROM unidb_students ORDER BY unidb_students.mentor;

SELECT CONCAT(unidb_lecturers.fname, unidb_lecturers.lname), unidb_lecturers.office FROM unidb_lecturers ORDER BY unidb_lecturers.office;

SELECT CONCAT(unidb_lecturers.fname, unidb_lecturers.lname), unidb_lecturers.staff_no FROM unidb_lecturers WHERE staff_no > 500;

SELECT CONCAT(unidb_students.fname, unidb_students.lname), unidb_students.id FROM unidb_students WHERE unidb_students.id > 1668 AND unidb_students.id < 1824;

SELECT CONCAT(unidb_students.fname, unidb_students.lname), unidb_students.country FROM unidb_students WHERE country = "US" OR country = "AU" OR country = "NZ";

SELECT CONCAT(unidb_lecturers.fname, unidb_lecturers.lname), unidb_lecturers.office FROM unidb_lecturers WHERE unidb_lecturers.office LIKE "g%";

SELECT CONCAT(unidb_courses.dept, unidb_courses.num) FROM unidb_courses WHERE unidb_courses.dept != "COMP";

SELECT CONCAT(unidb_students.fname, unidb_students.lname), unidb_students.country FROM unidb_students WHERE unidb_students.country = "FR" OR unidb_students.country = "MX";


